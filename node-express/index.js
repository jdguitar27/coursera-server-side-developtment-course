const express = require('express');
// const http = require('http');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const session = require('express-session');
const fileStore = require('session-file-store')(session);

const dishRouter = require('./routes/dishRouter');
const leaderRouter = require('./routes/leaderRouter');
const promoRouter = require('./routes/promoRouter');
const usersRouter = require('./routes/users');
const hostname = 'localhost';
const port = 3000;

const app = express();

require('./database');

app.use(morgan('dev'));
app.use(bodyParser.json());

app.use(session({
  name: 'session-id',
  secret: '12345-67890-09876-54321',
  saveUninitialized: false,
  resave: false,
  store: new fileStore()
}));

app.use('/users', usersRouter);

function auth(req, res, next) {
  if (req.session.user === 'authenticated')
    next();
  else {
    var err = new Error('You are not authenticated!');
    err.status = 403;
    return next(err);
  }
}

app.use(auth);

app.use(express.static(__dirname + '/public'));

app.use('/dishes', dishRouter);
app.use('/leaders', leaderRouter);
app.use('/promotions', promoRouter);

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(err.status || 500).send(err.message);
});

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}`);
});