const express = require('express');
const Leaders = require('../models/leader');

const leaderRouter = express.Router();

/*
I use the 'async/await' method to handle promises instead of .then. 
Is a feature of javascript intoduced in ES8. 
You can learn more about this in the next link: 
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
*/ 
leaderRouter.route('/')
.get(async (req, res, next) => {
    try
    {
        const leaders = await Leaders.find({});
        res.statusCode = 200,
        res.setHeader('Content-Type', 'application/json');
        res.json(leaders);    
    }
    catch (err) 
    {
        next(err);
    }  
})
.post(async (req, res, next) => {
    try
    {
        const leader = await Leaders.create(req.body);
        res.statusCode = 200,
        res.setHeader('Content-Type', 'application/json');
        res.json(leader);    
    }
    catch (err) 
    {
        next(err);
    }
})
.delete(async (req, res, next) => {
    try
    {
        const response = await Leaders.remove({});
        res.statusCode = 200,
        res.setHeader('Content-Type', 'application/json');
        res.json(response);    
    }
    catch (err) 
    {
        next(err);
    }
});

leaderRouter.route('/:id')
.get(async (req, res, next) => {
    try
    {
        const leader = await Leaders.findById(req.params.id);
        if(leader != null)
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(leader);
        }
        else
        {
            error = new Error(`Leader ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }    
    }
    catch (err) 
    {
        next(err);
    }
})
.put(async (req, res, next) => {
    try
    {
        const leader = await Leaders.findByIdAndUpdate(req.params.id, req.body);
        if(leader != null)
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(leader);
        }
        else
        {
            error = new Error(`Leader ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }    
    }
    catch (err) 
    {
        next(err);
    }
})
.delete(async (req, res, next) => {
    try
    {
        const leader = await Leaders.findByIdAndDelete(req.params.id);
        if(leader != null)
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(leader);
        }
        else
        {
            error = new Error(`Leader ${req.params.id} not found`);
            error.status = 404;
            throw error;
        } 
            
    }
    catch (err) 
    {
        next(err);
    }
});

module.exports = leaderRouter;