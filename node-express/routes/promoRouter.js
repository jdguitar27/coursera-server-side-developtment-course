const express = require('express');
const Promotions = require('../models/promotion');

const promoRouter = express.Router();

/*
    I use the 'async/await' method to handle promises instead of .then. 
    Is a feature of javascript intoduced in ES8. 
    You can learn more about this in the next link: 
    https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
*/ 
promoRouter.route('/')
.get(async (req, res, next) => {
    try
    {
        const promotions = await Promotions.find({});
        res.statusCode = 200,
        res.setHeader('Content-Type', 'application/json');
        res.json(promotions);    
    }
    catch (err) 
    {
        next(err);
    }  
})
.post(async (req, res, next) => {
    try
    {
        const promo = await Promotions.create(req.body);
        res.statusCode = 200,
        res.setHeader('Content-Type', 'application/json');
        res.json(promo);    
    }
    catch (err) 
    {
        next(err);
    }
})
.delete(async (req, res, next) => {
    try
    {
        const response = await Promotions.remove({});
        res.statusCode = 200,
        res.setHeader('Content-Type', 'application/json');
        res.json(response);    
    }
    catch (err) 
    {
        next(err);
    }
});

promoRouter.route('/:id')
.get(async (req, res, next) => {
    try
    {
        const promo = await Promotions.findById(req.params.id);
        if(promo != null)
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(promo);
        }
        else
        {
            error = new Error(`Promotion ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }    
    }
    catch (err) 
    {
        next(err);
    }
})
.put(async (req, res, next) => {
    try
    {
        const promo = await Promotions.findByIdAndUpdate(req.params.id, req.body);
        if(promo != null)
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(promo);
        }
        else
        {
            error = new Error(`Promotion ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }    
    }
    catch (err) 
    {
        next(err);
    }
})
.delete(async (req, res, next) => {
    try
    {
        const promo = await Promotions.findByIdAndDelete(req.params.id);
        if(promo != null)
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(promo);
        }
        else
        {
            error = new Error(`Promotion ${req.params.id} not found`);
            error.status = 404;
            throw error;
        } 
            
    }
    catch (err) 
    {
        next(err);
    }
});

module.exports = promoRouter;