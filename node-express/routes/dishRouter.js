const express = require('express');
const Dishes = require('../models/dishes');

const dishRouter = express.Router();

/*
    I use the 'async/await' method to handle promises instead of .then. 
    Is a feature of javascript intoduced in ES8. 
    You can learn more about this in the next link: 
    https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
*/ 
dishRouter.route('/')
.get(async (req, res, next) => {
    try
    {
        const dishes = await Dishes.find({});
        res.statusCode = 200,
        res.setHeader('Content-Type', 'application/json');
        res.json(dishes);    
    }
    catch (err) 
    {
        next(err);
    }  
})
.post(async (req, res, next) => {
    try
    {
        const dish = await Dishes.create(req.body);
        res.statusCode = 200,
        res.setHeader('Content-Type', 'application/json');
        res.json(dish);    
    }
    catch (err) 
    {
        next(err);
    }
})
.delete(async (req, res, next) => {
    try
    {
        const response = await Dishes.remove({});
        res.statusCode = 200,
        res.setHeader('Content-Type', 'application/json');
        res.json(response);    
    }
    catch (err) 
    {
        next(err);
    }
});

dishRouter.route('/:id')
.get(async (req, res, next) => {
    try
    {
        const dish = await Dishes.findById(req.params.id);
        if(dish != null)
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(dish);     
        }
        else
        {
            error = new Error(`Dish ${req.params.id} not found`);
            error.status = 404;
            throw error;
        } 
    }
    catch (err) 
    {
        next(err);
    }
})
.put(async (req, res, next) => {
    try
    {
        const dish = await Dishes.findByIdAndUpdate(req.params.id, req.body);
        if(dish != null)
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(dish);     
        }
        else
        {
            error = new Error(`Dish ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }    
    }
    catch (err) 
    {
        next(err);
    }
})
.delete(async (req, res, next) => {
    try
    {
        const dish = await Dishes.findByIdAndDelete(req.params.id);
        if(dish != null)
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(dish);     
        }
        else
        {
            error = new Error(`Dish ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }    
    }
    catch (err) 
    {
        next(err);
    }
});

// Comments
dishRouter.route('/:id/comments')
.get(async (req, res, next) => {
    try
    {
        const dish = await Dishes.findById(req.params.id);
        if(dish != null)
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(dish.comments);
        }
        else
        {
            error = new Error(`Dish ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }    
    }
    catch (err) 
    {
        next(err);
    }  
})
.post(async (req, res, next) => {
    try
    {
        var dish = await Dishes.findById(req.params.id);
        if(dish != null)
        {
            dish.comments.push(req.body);
            dish = await dish.save();
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(dish.comments);
        }
        else
        {
            error = new Error(`Dish ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }    
    }
    catch (err) 
    {
        next(err);
    }
})
.delete(async (req, res, next) => {
    try
    {
        var dish = await Dishes.findById(req.params.id);
        if(dish != null)
        {
            for(var i = (dish.comments.length - 1); i >= 0; i--)
                dish.comments.id(dish.comments[i]._id).remove();

            dish = await dish.save();
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(dish.comments);
        }
        else
        {
            error = new Error(`Dish ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }    
    }
    catch (err) 
    {
        next(err);
    }
});

dishRouter.route('/:id/comments/:commentId')
.get(async (req, res, next) => {
    try
    {
        const dish = await Dishes.findById(req.params.id);
        if(dish != null && dish.comments.id(req.params.commentId))
        {
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(dish.comments.id(req.params.commentId));
        }
        else if(dish == null)
        {
            error = new Error(`Dish ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }
        else
        {
            error = new Error(`Comment ${req.params.commentId} not found`);
            error.status = 404;
            throw error;
        }  
    }
    catch (err) 
    {
        next(err);
    }  
})
.put(async (req, res, next) => {
    try
    {
        var dish = await Dishes.findById(req.params.id);
        if(dish != null && dish.comments.id(req.params.commentId))
        {
            if(req.body.rating)
                dish.comments.id(req.params.commentId).rating = req.body.rating;
            if(req.body.comment)
                dish.comments.id(req.params.commentId).comment = req.body.comment;
            
            dish = await dish.save();
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(dish.comments.id(req.params.commentId));
        }
        else if(dish == null)
        {
            error = new Error(`Dish ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }
        else
        {
            error = new Error(`Comment ${req.params.commentId} not found`);
            error.status = 404;
            throw error;
        }  
    }
    catch (err) 
    {
        next(err);
    } 
})
.delete(async (req, res, next) => {
    try
    {
        var dish = await Dishes.findById(req.params.id);
        if(dish != null && dish.comments.id(req.params.commentId))
        {
            dish.comments.id(req.params.commentId).remove();
            dish = await dish.save();
            res.statusCode = 200,
            res.setHeader('Content-Type', 'application/json');
            res.json(dish);
        }
        else if(dish == null)
        {
            error = new Error(`Dish ${req.params.id} not found`);
            error.status = 404;
            throw error;
        }
        else
        {
            error = new Error(`Comment ${req.params.commentId} not found`);
            error.status = 404;
            throw error;
        }
    }  
    catch (err) 
    {
        next(err);
    }
});

module.exports = dishRouter;