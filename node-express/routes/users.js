const express = require('express');
const User = require('../models/user');

const userRouter = express.Router();

userRouter.get('/', async (req, res, next) => {
    const users = await User.find();
    res.json({users});
});

userRouter.post('/signup', async (req, res, next) => {
    try {
        const { username, password } = req.body;
        let user = await User.findOne({ username });

        if (!user) {
            user = await User.create({
                username,
                password
            });

            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json({
                status: 'Registration Successful',
                user
            });
        }

        const err = new Error(`User ${username} already exists`);
        err.status = 403;
        throw err;

    } catch (error) {
        next(error);
    }
});

userRouter.post('/login', async (req, res, next) => {
    let err;
    try {
        if (!req.session.user) {
            const authHeader = req.headers.authorization;

            if (!authHeader) {
                err = new Error('You are not authenticated!');
                res.setHeader('WWW-Authenticate', 'Basic');
                err.status = 401;
                throw err;
            }

            const auth = new Buffer.from(authHeader.split(' ')[1], 'base64').toString().split(':');
            const username = auth[0];
            const password = auth[1];
            const user = await User.findOne({ username });

            if(!user)
            {
                err = new Error(`User ${username} does not exists`);
                err.status = 403;
                throw err;
            } else if(user.password !== password)
            {
                err = new Error( 'Your password is incorrect!');
                err.status = 403;
                throw err;
            } else if(user.password === password)
            {
                req.session.user = 'authenticated';
                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/plain');
                res.end('You are authenticated!');
            }
        } else {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/plain');
            res.end('You are already authenticated!');
        }
    } catch (error) {
        next(error);
    }
});

userRouter.get('/logout', (req, res, next) => {
    if(req.session) {
        req.session.destroy();
        res.clearCookie('session-id');
        res.redirect('/');
    } else {
        let err = new Error('You are not logged in!');
        err.status = 403;
        next(err);
    }
});

module.exports = userRouter;