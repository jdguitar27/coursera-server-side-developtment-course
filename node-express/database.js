const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/conFusion', {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false
})
    .then(db => console.log('Database conected to the server'))
    .catch(err => console.log(err));